# yaml-language-server: $schema=../../schema.json
Name: Worker Queues Process High Activity Triggers
Id: '3060'
Type: labs
Enabled: true
Objective:
  Ensure timely trigger processing by creating a dynamic pool of workers to
  handle requests
Description: |+
  From this lab, the user will use a cluster of workers to handle
  a set of WorkOrders in parallel.

  **Business ROI**: Dynamic job processing provideds a scalable, general purpose way to automate routine tasks
Time: 10
Difficulty: advanced
Prereqs:
  Labs:
    - '1010'
    - '3010'
Concepts:
  - trigger
  - contexts
  - clusters
Tags:
  - core
LabSection:
  Name: Summary
  Sections:
    - Name: Create Cluster of workers
      Sections:
        - Objective: From the [Clusters](ux://clusters) view, click `Add`
          Description: |-
            Set `Name` to `lab3060`

            Set `Required Params` `broker/name` to `context-broker`

            Click `Save`
        - Objective: Wait for cluster to complete building
    - Name: Put Cluster Machines in WorkOrder mode
      Sections:
        - Objective: Go to the [Machines](ux://machines) view
        - Objective: Select all machines that start with "`lab3060-`"
        - Objective:
            Hover over the `Actions` dropdown and select `Work Order Mode`
          Description:
            All machines should enter WorkOrder mode and show pending/running
            Work Orders in the table.
    - Name: Update lab3010 Trigger
      Sections:
        - Objective: Go to [lab3010](ux://triggers/lab3010) editor
        - Objective: Change `Filter` to `cluster/tags:In:lab3060`
        - Objective: Set `Queue Requests` to `false`
        - Objective:
            Under `Work Order Config`, add a parameter, `dev/wait-time` with a
            value of `1`
          Description: Click a white space area to make sure all items are saved
    - Name: Distribute and Run Work Orders
      Sections:
        - Objective: Go to the [Machines](ux://machines) view
        - Objective:
            Click the `lab3010` Trigger Button at least 10 times quickly
        - Objective:
            Notice the `lab3060-*` machines having pending and running work
            orders
          Description: |+
            The Work Orders are distributed across the workers in an even fashion and the workers will
            run the Work Orders taking their next pending Work Order once finished with the current one.

            In the Queue Requests `false` mode for the trigger, the Work Orders are assigned when they are created.
        - Objective: Let the Work Orders complete
          Description: |+
            All machines should have '0's showing for pending and running Work Orders.
    - Name: Convert to Queue Mode for the Trigger.
      Sections:
        - Objective: Go to [lab3010](ux://triggers/lab3010) editor
        - Objective: Set `Queue Requests` to `true`
    - Name: Queue Work Orders to the Machines
      Sections:
        - Objective: Go to the [Machines](ux://machines) view
        - Objective:
            Click the `lab3010` Trigger Button at least 10 times quickly
        - Objective:
            Noitce the `lab3060-*` machines having pending and running work
            orders
          Description: |+
            The Work Orders are distributed to all machines and each machine will run a Work Order order
            from the queue when it can.  This cause lets the work distribute to the machines based upon
            their availablity.
        - Objective: Let the Work Orders complete
          Description: |+
            All machines should have '0's showing for pending and running Work Orders.
