#!/usr/bin/env bash

set -e

. tools/version.sh
version="v${MajorV}.${MinorV}.${PatchV}${Extra}"
branch=${MajorV}.${MinorV}

DOIT=0
if [[ $version =~ ^v ]] ; then
    DOIT=1
fi
if [[ $DOIT == 0 ]] ;then
    echo "Not a publishing branch."
    exit 0
fi

cd docs/labs
find . -type f | grep -v labs.filelist | sort > labs.filelist.tmp
sed "s/\./https:\/\/rebar-catalog.s3-us-west-2.amazonaws.com\/docs\/${branch}\/labs/" labs.filelist.tmp > labs.filelist
rm -f labs.filelist.tmp
cd -

mkdir -p rebar-catalog/labs/internal
cp -r labs.json rebar-catalog/labs/$version.json
cp -r internal-labs.json rebar-catalog/labs/internal/$version.json
if [[ "$CI_COMMIT_TAG" != "" && "$CI_COMMIT_TAG" != "$version" ]] ; then
  cp -r labs.json rebar-catalog/labs/$CI_COMMIT_TAG.json
  cp -r internal-labs.json rebar-catalog/labs/internal/$CI_COMMIT_TAG.json
fi
if [[ "$version" != "latest" && "$CI_COMMIT_TAG" != "latest" ]] ; then
  ltag=$(git rev-list -n 1 latest)
  vtag=$(git rev-list -n 1 $CI_COMMIT_TAG 2>/dev/null || :)
  if [[ "$ltag" == "$vtag" ]] ; then
    cp -r labs.json rebar-catalog/labs/latest.json
    cp -r internal-labs.json rebar-catalog/labs/internal/latest.json
  fi
fi

mkdir -p rebar-catalog/docs/$branch/images
cp README.rst rebar-catalog/docs/$branch
cp -r images/* rebar-catalog/docs/$branch/images
cp -r docs/* rebar-catalog/docs/$branch

