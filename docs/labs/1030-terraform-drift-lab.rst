.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Detect Terraform Drift; Labs

.. _rs_cp_1030:

1030 Detect Terraform Drift
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1030
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: terraform, cloud, clusters, compliance
* Concepts: clusters, resource brokers
..  youtube:: bjlFL8Y0p3U
    :privacy_mode:
`Video Link <https://youtu.be/bjlFL8Y0p3U>`__

Objective
---------

Detect when cloud resources are changed outside of Digital Rebar


  **Business ROI**: Improved operational control and consistency of
  infrastructure


Prerequisites
-------------
Required Labs:

* 1020

Addtional Checklist Items:

* Access to your cloud provider console


Summary
=======

Force drift into the `lab1020 <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1020>`__ Cluster
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

   
   
   
      
      
      
      #. Verify that you can find the associated instances in your cloud provider
         console
      
      
      
      
      #. Make sure that the cluster has completed provisioning and entered Work
         Order mode
      
      
      
      
      #. Remove some (not all) of the provisioned resources using your cloud
         provider’s CLI or UX
      
   

Detect drift from the `lab1020 <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1020>`__ cluster
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Select and apply the ``cloud-cluster-drift-detection`` blueprint
   
   
   
   
   #. Review the `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ to see the WARNING created by
      Digital Rebar detecting the drift
   


Create a drift detection trigger
++++++++++++++++++++++++++++++++
   
   
   
   #. Create a new `trigger <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ named lab1030
   
   
   
   
   #. Select the ```cron-trigger`` Trigger
      Provider <https://portal.rackn.io/#/e/0.0.0.0/trigger_providers/cron-trigger>`__
   
      The options will change when you select the provider
   
   
   
   
   #. Configure the new trigger
   
      Select the ``cloud-cluster-drift-detection`` blueprint.
      
      Select ``clusters`` as the filter to regularly scan for drift.
      
      Set the ``cron-trigger/time-string`` to your preferred frequency.
      
      -  Selecting Hour: ``1`` will create a daily scan at 1 am.
      -  Selecting Minute: ``1`` will create an hourly scan at :01 each hour.
      
      Save the Trigger.
   
   
   
   
   #. Update the ``All Filter Machines`` value to ``true``
   
      This ensures that the trigger runs on all active clusters.
   



