.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Create an Alert when Content is updated; Labs

.. _rs_cp_1050:

1050 Create an Alert when Content is updated
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1050
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: alerts, triggers, compliance, orchestration
* Concepts: triggers, alerts, content packs
..  youtube:: -9Xl8fI_6MI
    :privacy_mode:
`Video Link <https://youtu.be/-9Xl8fI_6MI>`__

Objective
---------

Be alerted when content is updated in a Digital Rebar endpoint


  **Business ROI**: Operators must be able to track and verify that
  environments have only been updated by planned processes


Prerequisites
-------------
Required Labs:

* 1005



Summary
=======

Inspect your system
+++++++++++++++++++
   
   
   
   #. Navigate to `System Information and Preferences <https://portal.rackn.io/#/e/0.0.0.0/system>`__ for the
      list of API Objects at the bottom right
   
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ to find the Digital Rebar
      Endpoint Self-Runner
   
      The name will match the Digital Rebar Endpoint ID in the top left
      corner.
   
   
   
   
   #. Open The Live Event Panel
   
      The Live Event Panel can be opened by clicking the megaphone icon on top
      of the side nav, next to the Digital Rebar Endpoint ID.
   
   
   
   
   #. Navigate to `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__ and update or install the
      `“dev-library” <https://portal.rackn.io/#/e/0.0.0.0/catalog/dev-library>`__ Content Pack or Plugin from
      the library
   
      Observe the “CREATE contents dev-library” event in the Live event Panel
   
   
   
   
   #. Navigate to `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ page and create a new Trigger
   
      Name: lab1050
      
      Provider: ``event-trigger``
      
      Blueprint: ``alerts-on-content-change``
      
      Filter to the Local Self Runner
      
      Param ``event-trigger/event-match`` should be set to ``contents.*.*``
      
      Save
      
      Set MergeData: ``true``
   
   
   
   
   #. Navigate to `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__ and update or install a Content
      Pack from the library
   
      The specific Content Pack installed does not matter as the action of
      installing one will run the Trigger configured in the previous steps.
   
   
   
   
   #. Navigate to `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ to review the created Alert(s)
   
      Acknowledge the alerts(s) by clicking on the bell icon in the leftmost
      column or clicking into the alert and clicking “Acknowledge Alert” on
      the top right.
   


Return to the `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ page to elevate the alert
level
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Open the lab1050 trigger
   
   
   
   
   #. In the ``Work Order Config`` params list, add the ``alert/level``
      Parameter with value ``WARN``
   
      This is done in the “Work Order Config” params at the bottom of the
      Editor tab instead of the “Params” tab as the Params tab is for Trigger
      specific params.
   
   
   
   
   #. Repeat the `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__ update process and note that the
      `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ are now WARN level
   



