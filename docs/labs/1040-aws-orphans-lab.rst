.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Find Orphaned AWS Instances; Labs

.. _rs_cp_1040:

1040 Find Orphaned AWS Instances
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1040
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: aws, cloud, compliance
* Concepts: tagged workers
..  youtube:: JQocsRKkqvI
    :privacy_mode:
`Video Link <https://youtu.be/JQocsRKkqvI>`__

Objective
---------

Discover AWS instances created outside of Digital Rebar


  **Business ROI**: Improved operational control and consistency of
  infrastructure


Prerequisites
-------------
Required Labs:

* 1030

Addtional Checklist Items:

* The AWS cloud broker installed
* At least one AWS instance running in your AWS region that was not
  created by Digital Rebar
* Access to your cloud provider console


Summary
=======

Create an AWS reference cluster
+++++++++++++++++++++++++++++++
   
   
   
   #. To provide a base-line, create a minimal cluster using the AWS resource
      broker (`lab 1020 <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/1020>`__)
   
   
   
   
   #. Allow it to create new AWS instances
   


Create AWS-CLI broker
+++++++++++++++++++++
   
   
   
   #. Create an AWS-CLI based `Resource Broker <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers>`__ with
      your AWS credentials and region
   
   
   
   
   #. Wait for the broker to enter Work Order mode
   


Run the Discovery Process
+++++++++++++++++++++++++
   
   
   
   #. From the `AWSCLI Broker <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers/Name:aws-cli>`__, select
      and apply the ``cloud-awscli-reconcile-instances`` blueprint
   
   
   
   
   #. Review the `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ to see the INFO created by Digital
      Rebar discovering unregistered machines
   
   
   
   
   #. Review the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ to see machines created by
      Digital Rebar based on your AWS-CLI broker configuration. Note that
      scans are region specific
   
   
   
   
   #. Review all AWS machines to confirm that the ``aws/inspect`` Param is
      populated with AWS discovery information
   



