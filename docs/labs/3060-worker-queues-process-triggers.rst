.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Worker Queues Process High Activity Triggers; Labs

.. _rs_cp_3060:

3060 Worker Queues Process High Activity Triggers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 3060
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: advanced
* Tags: core
* Concepts: trigger, contexts, clusters

Objective
---------

Ensure timely trigger processing by creating a dynamic pool of workers
to handle requests


  From this lab, the user will use a cluster of workers to handle a set of
  WorkOrders in parallel.
  
  **Business ROI**: Dynamic job processing provideds a scalable, general
  purpose way to automate routine tasks


Prerequisites
-------------
Required Labs:

* 1010
* 3010



Summary
=======

Create Cluster of workers
+++++++++++++++++++++++++
   
   
   
   #. From the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ view, click ``Add``
   
      Set ``Name`` to ``lab3060``
      
      Set ``Required Params`` ``broker/name`` to ``context-broker``
      
      Click ``Save``
   
   
   
   
   #. Wait for cluster to complete building
   


Put Cluster Machines in WorkOrder mode
++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Go to the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view
   
   
   
   
   #. Select all machines that start with “``lab3060-``”
   
   
   
   
   #. Hover over the ``Actions`` dropdown and select ``Work Order Mode``
   
      All machines should enter WorkOrder mode and show pending/running Work
      Orders in the table.
   


Update lab3010 Trigger
++++++++++++++++++++++
   
   
   
   #. Go to `lab3010 <https://portal.rackn.io/#/e/0.0.0.0/triggers/lab3010>`__ editor
   
   
   
   
   #. Change ``Filter`` to ``cluster/tags:In:lab3060``
   
   
   
   
   #. Set ``Queue Requests`` to ``false``
   
   
   
   
   #. Under ``Work Order Config``, add a parameter, ``dev/wait-time`` with a
      value of ``1``
   
      Click a white space area to make sure all items are saved
   


Distribute and Run Work Orders
++++++++++++++++++++++++++++++
   
   
   
   #. Go to the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view
   
   
   
   
   #. Click the ``lab3010`` Trigger Button at least 10 times quickly
   
   
   
   
   #. Notice the ``lab3060-*`` machines having pending and running work orders
   
      The Work Orders are distributed across the workers in an even fashion
      and the workers will run the Work Orders taking their next pending Work
      Order once finished with the current one.
      
      In the Queue Requests ``false`` mode for the trigger, the Work Orders
      are assigned when they are created.
   
   
   
   
   #. Let the Work Orders complete
   
      All machines should have ’0’s showing for pending and running Work
      Orders.
   


Convert to Queue Mode for the Trigger.
++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Go to `lab3010 <https://portal.rackn.io/#/e/0.0.0.0/triggers/lab3010>`__ editor
   
   
   
   
   #. Set ``Queue Requests`` to ``true``
   


Queue Work Orders to the Machines
+++++++++++++++++++++++++++++++++
   
   
   
   #. Go to the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view
   
   
   
   
   #. Click the ``lab3010`` Trigger Button at least 10 times quickly
   
   
   
   
   #. Noitce the ``lab3060-*`` machines having pending and running work orders
   
      The Work Orders are distributed to all machines and each machine will
      run a Work Order order from the queue when it can. This cause lets the
      work distribute to the machines based upon their availablity.
   
   
   
   
   #. Let the Work Orders complete
   
      All machines should have ’0’s showing for pending and running Work
      Orders.
   



