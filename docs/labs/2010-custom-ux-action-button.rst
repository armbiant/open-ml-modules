.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Create a Custom UX Action Button; Labs

.. _rs_cp_2010:

2010 Create a Custom UX Action Button
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 2010
* Time: 5 Minutes
* Enabled: Yes
* Difficulty: intermediate
* Tags: self-service, triggers
* Concepts: triggers, alerts, ux
..  youtube:: u22KS_QU4jI
    :privacy_mode:
`Video Link <https://youtu.be/u22KS_QU4jI>`__

Objective
---------

Learn how to extend the UX by adding a programmable button based on a
trigger


  In this lab, the user will create a custom UX button that will generate
  a trigger.
  
  **Business ROI:** Improve self-service and consistency by making common
  operations more accessible to all operators.


Prerequisites
-------------
Required Labs:

* 1050



Summary
=======

Create Trigger
++++++++++++++
   
   
   
   #. Navigate to the `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ page and click the
      ``Create`` button
   
      Set ``Name`` to ``lab2010``
      
      Set ``Trigger Provider`` to ``event-trigger``
      
      Set ``Blueprint`` to ``utility-endpoint-systems-check``
      
      Set ``Filter`` to ``Local Self Runner``
      
      Add a Parameter ``event-trigger/event-match`` with a value of
      ``ux.button.lab2010``
      
      Click ``Save``.
   
   
   
   
   #. Refresh your UX page (Ctrl-F5 or Ctrl-R)
   
      The UX feature that adds new Trigger Action Buttons requires a page
      refresh.
   


Test the New Button
+++++++++++++++++++
   
   
   
   #. Navigate to the `Work Orders <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__ view
   
   
   
   
   #. Click the ``lab2010`` button on the top right of the top nav.
   
      This may be under a “Trigger Buttons” dropdown next the labs button.
   
   
   
   
   #. Observe new Work Orders being created
   
      Look for the newly created Work Order when you click the button in the
      table.
   


Add the Dev Library Content Pack
++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__
   
   
   
   
   #. Find the ```dev-library`` <https://portal.rackn.io/#/e/0.0.0.0/catalog/dev-library>`__ using the search
      bar
   
   
   
   
   #. Install or Update ``dev-library`` if not loaded, by clicking the install
      button
   


Create Another Trigger
++++++++++++++++++++++
   
   
   
   #. From the `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ page, click the ``Create`` button
   
      Set ``Name`` to ``lab2010-dev``
      
      Set ``Trigger Provider`` to ``event-trigger``
      
      Set ``Blueprint`` to ``dev-raise-alerts``
      
      Set ``Filter`` to ``Local Self Runner``
      
      Add a Parameter ``event-trigger/event-match`` with a value of
      ``ux.button.lab2010-dev``
      
      Click ``Save``
   
   
   
   
   #. Refresh the Portal (Ctrl-F5 or Ctrl-R)
   


See Triggers that Generate Alerts
+++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ table
   
   
   
   
   #. Click the Lab2010-dev button on the top right of the banner
   
   
   
   
   #. Observe new Alerts being created in the table
   
   
   
   
   #. Look for the newly created Alerts when the button is clicked
   



