.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Run Ad Hoc tasks on Machines; Labs

.. _rs_cp_2080:

2080 Run Ad Hoc tasks on Machines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 2080
* Time: 15 Minutes
* Enabled: Yes
* Difficulty: intermediate
* Tags: core, day2
* Concepts: tasks, blueprints, work_orders

Objective
---------

Create and run ad hoc tasks on running machines without needing to run a
full workflow process.


  Any Digital Rebar operation (aka “task”) can be run as an immediate
  request on a machine, cluster or resource broker. This process uses the
  Work Order mode of machines and can be standardized using Blueprints.
  
  This lab shows how to run this process by creating a custom Task and
  Blueprint. It includes using Params to inject configuration into
  processes dynamically.
  
  Business ROI: Allows standardized and reusable actions for running
  systems (Day 2 operations)


Prerequisites
-------------
Required Labs:

* 1010



Summary
=======

Create a Cluster of Machines in Work Order Mode
+++++++++++++++++++++++++++++++++++++++++++++++

   Before we can create tasks in blueprints, we need to create machines
   that will run the work orders. We do this by creating a cluster of
   machines that are in WorkOrder mode.
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2080``
      
      Set the ``broker/name`` to ``context-broker``
      
      Set the ``cluster/count`` to 0
      
      Click ``Save`` and wait for it to enter Work Order Mode
   
   
   
   
   #. Wait for `Activity <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab2080/activity>`__ to complete.
   
   
   
   
   #. Navigate to Lab’s `dedicated “lab2080” Profile
      tab <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab2080/profiles/lab2080>`__ or `it’s
      profile <https://portal.rackn.io/#/e/0.0.0.0/profiles/lab2080>`__
   
      Add Param ``on-complete-work-order-mode``
      
      Set the ``on-complete-work-order-mode`` to ``true``
      
      Set the ``cluster/count`` to 3
   
   
   
   
   #. Navigate to the `Lab’s activity
      tab <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab2080/activity>`__
   
      The re-evaluate the cluster by Applying the
      ``universal-application-base-cluster`` Blueprint in the “Work Order
      Management” section.
   
   
   
   
   #. Observe the `Lab2080-\* Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
      All should created with in ``Work Order Mode`` (clipboard icon) enabled.
      
      Note that the ``Workflow`` and ``Stage`` fields are cleared and their
      queues are empty.
   


Create a Task
+++++++++++++

   Tasks are the primary way that Digital Rebar does work and operate by
   creating files that can be used by the system. That is typically simple
   bash or Powershell, but can include complex template files such as
   Terraform plans or YAML configuration files.
   
   **This task will be used in later labs**
   
   
   
   #. Navigate to `Tasks <https://portal.rackn.io/#/e/0.0.0.0/tasks>`__
   
   
   
   
   #. Click ``Create``
   
      Set ``Name`` to ``lab2080``
      
      Click ``New Template +`` to create code area (#0) for the task.
      
      Leave the Name (``template_0``) and Path (empty) as default.
      
      In the #0 editing area, add the example code.
      
      ::
      
         #!/bin/bash
         # Lab 2080
         echo "=== General Setup ==="
         {{template "setup.tmpl" .}}
         echo "== Lab 2080 ==="
         echo "{{.Machine.Name}}" >> lab2080.txt
         cat lab2080.txt
         wait=$((1 + $RANDOM % 10))
         echo "=== Random Wait $wait ==="
         sleep $wait
         echo "=== Done ==="
         exit 0
      
      This example, code will run the 1) common setup template, 2) create a
      file with the machine’s name, and 3) the wait up to 10 seconds.
      
      Click Save
   


Create and Run the Blueprint
++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Blueprints <https://portal.rackn.io/#/e/0.0.0.0/blueprints>`__
   
      Click ``Create``
      
      Set ``Name`` to ``lab2080``
      
      Click ``Add Task +`` and choose the ``lab2080`` task that you created
      earlier
      
      Click Continue
      
      Navigate to the `Blueprint’s Apply
      tab <https://portal.rackn.io/#/e/0.0.0.0/blueprints/lab2080/resources>`__
      
      Select all the ``lab2080-*`` machines.
      
      Click ``Apply lab2080`` multiple times to watch the Blueprint be queued
      and processed on all the machines in parallel.
   
   
   
   
   #. Execute the Blueprint from a Machine
   
      Navigate to one of your `Lab 2080
      Machines <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0>`__
      
      In the “Work Order Management” section, select and apply the ``lab2080``
      blueprint
      
      Go to the `Activity tab <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0/activity>`__ to
      watch the work order complete. Note that you can also run the blueprint
      from the Activity tab.
      
      Expand the most recent Activity to see the job log. You’ll see that task
      was executed and added the machine name.
   
   
   
   
   #. Run Blueprint from the CLI (optional)
   
      For Day 2 IaC operations, you can easily queue work orders directly
      against a machine. These can be ad hoc work orders or pre-defined
      blueprints. For
      
      Since we’ve defined a blueprint, we’ll simply apply that blueprint to
      one of our machines as we’ve demostrated via the UX.
      
      To run the lab2080 blueprint on the lab2080-0 machine, you can use the
      machines work order management commands of the DRPCLI.
      
      ``drpcli machines work_order add Name:lab2080-0 lab2080``
      
      This command will return the created Work Order as JSON.
   


Add a Required Param to Task
++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `lab2080 Task <https://portal.rackn.io/#/e/0.0.0.0/tasks/lab2080>`__
   
      Edit the task template in area ``#0``
      
      On **line 5**, replace the original line ``echo "== Lab 2080 ==="``
      with:
      
      ::
      
         echo "== {{ .Param "lab2080/note" }} ==="`
      
      This will allow you to substitute any name for the original text. We’re
      going to show you how this can fail in practice and then show you how to
      make this safe practice.
   
   
   
   
   #. Watch failure of updated `lab2080
      Blueprint <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0/activity>`__
   
      Select and apply the ``lab2080`` blueprint to the machine.
      
      Notice that the task fails.
      
      Expand the task information and observe the Missing Parameter error
      message:
      
      *executing “template_0” at <.Param>: error calling Param: No such
      machine parameter lab2080/note*
      
      This error message is telling you that the Param you included was not
      available during processing.
   
   
   
   
   #. Create the `Param <https://portal.rackn.io/#/e/0.0.0.0/params>`__
   
      Navigate to the `Params <https://portal.rackn.io/#/e/0.0.0.0/params>`__ page.
      
      Click “create” and select a “String” type param.
      
      Name it “lab2080/note” to match the name used in the `lab2080
      task <https://portal.rackn.io/#/e/0.0.0.0/tasks/lab2080>`__.
      
      Click “save”
   
   
   
   
   #. Make Param required in `lab2080 Task <https://portal.rackn.io/#/e/0.0.0.0/tasks/lab2080>`__
   
      Return to the `lab2080 Task <https://portal.rackn.io/#/e/0.0.0.0/tasks/lab2080>`__.
      
      In the “Required Params” field, include the ``lab2080/note``.
      
      This will tell Digital Rebar to check for the existance of the Param
      before trying to render the task.
      
      While best practice is to provide a safe default for Params, we still
      recommend correctly identifying Required and Optional parameters in
      tasks. This IaC practice clearly communicates which Params are required
      and used in a task.
   
   
   
   
   #. Watch failure of (re)updated `lab2080
      Blueprint <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0/activity>`__
   
      Select and apply the ``lab2080`` blueprint to the machine.
      
      Notice that the task still fails but the error message has changed.
      
      Expand the task information and observe changed Missing Parameter error
      message:
      
      *ValidationError: Missing required parameter lab2080/note for tasks
      lab2080*
      
      This error message is telling you that the Required parameter for the
      Task was not provided.
   
   
   
   
   #. Add Param to correct failure of
      `lab2080-0 <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0/params>`__
   
      Add Param ``lab2080/note`` to the lab2080 machine.
      
      Set the ``lab2080/note`` param to ``fixed!`` (you can choose other
      comments if you’d like)
      
      Navigate to the `Activity
      tab <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0/activity>`__.
      
      Select and Apply the ``lab2080`` blueprint.
      
      Observe that the Work Order now completes successfully!
      
      Expand the Work Order to verify that the text from your param appears in
      the Task output.
   
   
   
   
   #. Set ``lab2080/note`` for all Machines in `lab2080
      Cluster <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab2080-0/profiles/lab2080>`__
   
      Navigate to the shared `lab2080 profile <https://portal.rackn.io/#/e/0.0.0.0/profiles/lab2080>`__
      directly or via the
      `machines <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2080-0/profiles/lab2080>`__ or
      `cluster <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab2080-0/profiles/lab2080>`__.
      
      Set the ``lab2080/note`` param to ``shared!`` (you can choose other
      comments if you’d like)
      
      Navigate to the `lab2080
      blueprint <https://portal.rackn.io/#/e/0.0.0.0/blueprints/lab2080/resources>`__.
      
      Select the lab2080 machines and Apply the ``lab2080`` blueprint.
      
      If you explore the `Jobs <https://portal.rackn.io/#/e/0.0.0.0/jobs>`__ created, you will notice that
      the param you set on the ``lab2080-0`` machine is used instead of the
      one you set in the shared profile. This is because the Digital Rebar
      uses a specific
      `precedence <https://docs.rackn.io/en/latest/doc/kb/kb-00057.html>`__
      when evaluating parameters. This IaC behavior is similar to variable
      scope in programming and is a common feature of automation systems.
   


Clean up
++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
      Check the ``lab2080`` cluster in the table
      
      Click ``Actions`` and then select ``Work Flow Mode``
      
      Click ``Actions`` and then select ``Delete (Cleanup)``
      
      Click ``Cleanup`` to confirm the action
      
      Wait for the clusters and machines to be removed
   



