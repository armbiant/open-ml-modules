.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Install Digital Rebar; Labs

.. _rs_cp_1000:

1000 Install Digital Rebar
~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1000
* Time: 15 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: core
* Concepts: install
..  youtube:: MqUl_Jxuv1Q
    :privacy_mode:
`Video Link <https://youtu.be/MqUl_Jxuv1Q>`__

Objective
---------

Install your own Digital Rebar test server


  **Business ROI** Unlike multi-tenant SaaS, Digital Rebar is user
  operated software: you’ll never have to open your firewall for us, share
  sensitive information or upload access credentials.


Prerequisites
-------------
**None**


Install Digital Rebar
=====================

Provision a Machine
+++++++++++++++++++

   Acquire a machine from your Virtual Machine Manager or Cloud Provider
   console.

.. tabs::

  .. tab:: Cloud or VM via Console

  
  
     These steps are also found in the `cloud
     install <https://docs.rackn.io/en/latest/doc/install/install-cloud.html>`__
     guide.
     
     #. Instantiate a Linux machine with a recent version (most major variants
        are supported)
     
     
     #. Have access to the Internet
     
        No access? see `Air Gap
        install <https://docs.rackn.io/en/latest/doc/install/install-airgap.html>`__
     
     
     #. Provide at least 2 CPUs and 8 GB of RAM for a lab system
     
     
     #. Make sure it has inbound access to SSH (22) and ports 8090-8092
     
        For example, use
        
        .. code:: sh
        
           firewall-cmd --permanent --add-port=22/tcp --add-port=8090-8092/tcp && firewall-cmd --reload
           firewall-cmd --add-port=22/tcp --add-port=8090-8092/tcp && firewall-cmd --reload
     
     
     #. Note the public IP address of the new instance
     
     
     #. SSH to your new cloud instance
     
  

  .. tab:: Terraform

  
  
     Cloud via Terraform uses steps in the `online Cloud
     install <https://docs.rackn.io/en/latest/doc/install/install-cloud.html#quickstart>`__
     guide
     
     Access to run `terraform <https://www.terraform.io/downloads>`__ with
     working AWS `credential and
     config <https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html>`__
     files are needed to successfully complete this lab.
     
     #. Download the cloud appropriate Terraform plan `AWS
        Cloud <https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/aws.tf?inline=false>`__,
        `Google <https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/google.tf?inline=false>`__,
        `Azure <https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/azure.tf?inline=false>`__,
        `Digital
        Ocean <https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/digitalocean.tf?inline=false>`__,
        or
        `Linode <https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/linode.tf?inline=false>`__
     
     
     #. Run the Terraform plan
     
        .. code:: sh
        
           terraform init
           terraform apply \
             -var="drp_username=rackn" -var="drp_password=myP4ssword"
        
        The plan defaults to using your public key located at
        ``~/.ssh/id_rsa.pub``. You can specify an alternate public key file by
        appending ``-var="ssh_pub_key=/path/to/key.pub"`` to the
        ``terraform apply`` command.
     
     
     #. Wait for Terraform to provide the DRP Server URL
     
     
     #. Skip to the **Login and Register** step
     
  

  .. tab:: PXE (Bare Metal and Local)

  
  
     The `Quick
     Start <https://docs.rackn.io/en/latest/doc/quickstart.html>`__ covers
     local Virtual Machine Manager or bare metal server.
     
     #. Instantiate a Linux machine with a recent version (most major variants
        are supported)
     
     
     #. Have network with access to the Internet
     
        No access? see `Air Gap
        install <https://docs.rackn.io/en/latest/doc/install/install-airgap.html>`__
     
     
     #. Have a DHCP isolated network
     
        For example in Virtualbox, provide a host only network with DHCP
        disabled.
     
     
     #. Provide at least 2 CPUs and 8 GB of RAM for a lab system
     
     
     #. Make sure it has inbound access to SSH (22) and ports 8090-8092
     
        For example, use
        
        .. code:: sh
        
           firewall-cmd --permanent --add-port=22/tcp --add-port=8090-8092/tcp && firewall-cmd --reload
           firewall-cmd --add-port=22/tcp --add-port=8090-8092/tcp && firewall-cmd --reload
     
     
     #. Note the access IP address of the new instance
     
     
     #. Console or SSH to your new machine
     
  


Install Digital Rebar
+++++++++++++++++++++

Choose the an install method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. tabs::

  .. tab:: General

  
     
     #. Run the following
     
        .. code:: sh
        
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal
     
  

  .. tab:: AWS

  
     
     #. Run the following or pass it into the User Data during instance create
     
        .. code:: sh
        
           value=$(curl -sfL http://169.254.169.254/latest/meta-data/public-ipv4)
           # install Digital Rebar
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
     
  

  .. tab:: Google

  
     
     #. Run the following or pass it into the User Data during instance create
     
        .. code:: sh
        
           value=$(curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
           # install Digital Rebar
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
     
  

  .. tab:: Azure

  
     
     #. Run the following or pass it into the User Data during instance create
     
        .. code:: sh
        
           value=$(curl -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2021-05-01&format=text")
           # install Digital Rebar
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
     
  

  .. tab:: Digital Ocean

  
     
     #. Run the following or pass it into the User Data during instance create
     
        .. code:: sh
        
           value=$(curl -sfL "http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address")
           # install Digital Rebar
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
     
  

  .. tab:: Linode

  
     
     #. Run the following
     
        .. code:: sh
        
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal
     
  

  .. tab:: Other Clouds

  
     
     #. Substitute the [public|access IP address] and run the following
     
        .. code:: sh
        
           value=[public|access IP address]
           curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
     
  


Alternative options
^^^^^^^^^^^^^^^^^^^

   Common additional install flags include
   
   +-----------------------------------+-----------------------------------+
   | Argument                          | Description                       |
   +===================================+===================================+
   | ``--version=tip``                 | Sets the install version to the   |
   |                                   | latest instead of the current     |
   |                                   | stable                            |
   +-----------------------------------+-----------------------------------+
   | ``--drp-user=[user]``             | Adds a new super user to the      |
   |                                   | endpoint                          |
   +-----------------------------------+-----------------------------------+
   | ``--drp-password=[pass]``         | Sets the password for the current |
   |                                   | user (rocketskates) or the new    |
   |                                   | user if specified.                |
   +-----------------------------------+-----------------------------------+
   | ``--remove-rocketskates``         | Removes the current rocketskates  |
   |                                   | user if different than            |
   |                                   | *–drp-user*                       |
   +-----------------------------------+-----------------------------------+
   | ``--drp-id=replaceme``            | Sets the ID of the DRP Endpoint   |
   |                                   | at install time                   |
   +-----------------------------------+-----------------------------------+



Login and Register
++++++++++++++++++

   You can login when you see the **Digital Rebar version** after
   ``Waiting for dr-provision to start`` completes
   
   
   
   #. Visit your server’s API url
   
      ``https://[public|access IP address]:8092``
   
   
   
   
   #. Accept the unsigned certificate
   
      This must be done initially until you install a signed certificate.
   
   
   
   
   #. Login using the system username and password
   
      Default is ``rocketskates`` and ``r0cketsk8ts``
   
   
   
   
   #. Complete the license form to create your license
   
      Or, upload your license if you’ve already created one.
   


Explore your system
+++++++++++++++++++

   We have a library of interactive labs to continue your journey!
   
   
   
   #. Visit `Info and Preferences <https://portal.rackn.io/#/e/0.0.0.0/system>`__ page to review the System
      Bootstrapping Wizard
   
      If you are using the default password, follow the “Change Default
      Password” link in the Wizard
   
   
   
   
   #. On the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ page, observe the self-runner
      machine that was created during the installation to process
   
      If you have loggged in before installation completes, you can observe
      Digital Rebar completing the bootstrapping process. workflows for your
      Digital Rebar service.
   
   
   
   
   #. On the `Jobs <https://portal.rackn.io/#/e/0.0.0.0/jobs>`__ page, observe the jobs that were run during
      the universal bootstrap process to setup the machine
   
   
   
   
   #. On the `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__ page, check out the versions of all
      the components in use by Digital Rebar
   
      If you used ``stable`` to install, you can upgrade to ``tip`` for the
      latest code from this page.
   
   
   
   
   #. Visit the `License Manager <https://portal.rackn.io/#/e/0.0.0.0/license>`__ page, to extend your
      no-obligation trial license for a 30 days
   



