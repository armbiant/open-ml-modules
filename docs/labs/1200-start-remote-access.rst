.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Start Remote Access; Labs

.. _rs_cp_1200:

1200 Start Remote Access
~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1200
* Time: 5 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: ssh, remote
* Concepts: machines

Objective
---------

Start Guacd Service to Allow Remote SSH Access to Systems


  **Business ROI**


Prerequisites
-------------
Required Labs:

* 1005

Addtional Checklist Items:

* Ensure an SSH server is running on the DRP endpoint (required for
  testing)
* Ensure the SSH port (22) is open for in-bound access (required for
  testing)


Remote Access
=============

Setup
+++++

Navigate to the Machine for the DRP Endpoint in the Portal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Navigate to the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view
   
   
   #. Open the editor for the DRP endpoint by clicking the name of the
      endpoint
   
      The name will match the Digital Rebar Endpoint ID in the top left
      corner.
   


Make sure the DRP Endpoint Machine is in unlocked
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Click the *Unlock Object* Button on the top right
   
      If *Lock Object* is present, then the machine is already unlocked.
      
      When a machine is locked, it is in a Read Only state and changes cannot
      be made.
   


Add the ``bootstrap-guacd`` Profile to the DRP Endpoint’s machine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Click the ``+`` icon by the *Profiles* line
   
      Skip to the next step if ``bootstrap-guacd`` is already in the list.
   
   
   #. Type and select ``bootstrap-guacd``
   


Re-run the bootstrap process
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Select the ``Activity`` Tab
   
   
   #. Click the ``Select Blueprint`` button and select by typing
      ``rebootstrap-drp``
   
   
   #. Click ``Apply``
   


Wait for the bootstrap process to finish.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Watch the ``Activity`` Tab for the *WorkOrder* to complete
   
   
   
      The DRP endpoint machine should finish all tasks.
   



Validation
++++++++++

Validate ``guacd-service`` is running
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Navigate to the `Resource Brokers view <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers>`__ to the
      ```guacd-service`` <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers/Name:guacd-service/activity>`__
   
   
   #. There should be a running and pending Work Order.
   


Connect to the DRP Endpoint with SSH
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   
   #. Navigate to the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ table
   
   
   #. Open the editor for the DRP endpoint by clicking the name of the
      endpoint
   
   
   #. Click the ``Remote`` button in upper right
   
      An active SSH session should start from within the Portal
   



Alternative Setup
+++++++++++++++++
   
   
   
   #. Add ``--initial-profiles=bootstrap-guacd`` to the ``install.sh`` line in
      Lab 1000 (Install Digital Rebar)
   
      The Remote Access System can be configured at install time
   



