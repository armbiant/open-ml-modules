.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Working with the Digital Rebar CLI; Labs

.. _rs_cp_2000:

2000 Working with the Digital Rebar CLI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 2000
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: intermediate
* Tags: fundamentals
* Concepts: cli, machines, events, alerts, catalog
..  youtube:: rpM0b8nqdVc
    :privacy_mode:
`Video Link <https://youtu.be/rpM0b8nqdVc>`__

Objective
---------

Learn how to use Digital Rebar by the command line for programmatic
control


  Using the Digital Rebar CLI, this lab will explore the ability to get
  and control DRP programmatically.
  
  **Business ROI**: Improved automation skills results in improved IaC
  contruction and integration.


Prerequisites
-------------
Required Labs:

* 1200



Explore ``drpcli``
==================

Setup CLI Environment
+++++++++++++++++++++

   This step will provide you a terminal to window / shell to do ``drpcli``
   commands.

.. tabs::

  .. tab:: DRP Shell

  
     
     #. Navigate to the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view
     
     
     #. Open the editor for the DRP endpoint machine
     
     
     #. Click the ``Remote`` button in the upper right of new panel
     
        The ``Remote`` button opens an SSH session from within the Portal.
     
     
     #. Resize the window for comfort
     
     
     #. Set the endpoint and access credentials
     
        During Lab 1000, Install Digital Rebar, you may have changed the access
        username and password. You will need to use those now to set an
        environment variable for accessing DRP. Since you are on the DRP
        Endpoint, the endpoint default value is sufficient.
        
        If you used the `Terraform
        Setup <https://docs.rackn.io/en/latest/doc/install/install-cloud.html#quickstart?utm_source=trial>`__,
        then these values will be provided
        
        .. code:: sh
        
           export RS_KEY=<username>:<password>
        
        For example:
        
        .. code:: sh
        
           export RS_KEY='rocketskates:r0cketsk8ts'
     
  

  .. tab:: Linux

  
     
     #. Open the `files <https://portal.rackn.io/#/e/0.0.0.0/files>`__ view
     
     
     #. Click ``drpcli.<arch>.linux`` where arch is amd64, arm64, or ppc64le to
        download
     
     
     #. Click the download button
     
     
     #. Open a terminal in your home directory
     
     
     #. Put the binary in place and make executable
     
        .. code:: sh
        
           cd ~/Downloads
           cp drpcli /usr/local/bin
           cp drpcli /usr/local/bin/jq
           chmod +x /usr/local/bin/drpcli
           chmod +x /usr/local/bin/jq
     
     
     #. Set the endpoint and access credentials
     
        During the lab1000, you may have changed the access username and
        password. You will need to use those now to set an environment variable
        for accessing DRP.
        
        If you used the `Terraform
        Setup <https://docs.rackn.io/en/latest/doc/install/install-cloud.html#quickstart?utm_source=trial>`__,
        then these values will be provided
        
        .. code:: sh
        
           export RS_KEY=<username>:<password>
           export RS_ENDPOINT=https://<DRP IP>:8092
        
        For example:
        
        .. code:: sh
        
           export RS_KEY='rocketskates:r0cketsk8ts'
           export RS_ENDPOINT=https://10.10.10.1:8092
     
  

  .. tab:: Darwin

  
     
     #. Open the `files <https://portal.rackn.io/#/e/0.0.0.0/files>`__ view
     
     
     #. Click ``drpcli.<arch>.darwin`` where arch is amd64 or arm64 to download
     
     
     #. Click the download button
     
     
     #. Open a terminal in your home directory
     
     
     #. Put the binary in place and make executable
     
        .. code:: sh
        
           cd ~/Downloads
           cp drpcli /usr/local/bin
           cp drpcli /usr/local/bin/jq
           chmod +x /usr/local/bin/drpcli
           chmod +x /usr/local/bin/jq
     
     
     #. Set the endpoint and access credentials
     
        During the lab1000, you may have changed the access username and
        password. You will need to use those now to set an environment variable
        for accessing DRP.
        
        If you used the `Terraform
        Setup <https://docs.rackn.io/en/latest/doc/install/install-cloud.html#quickstart?utm_source=trial>`__,
        then these values will be provided
        
        .. code:: sh
        
           export RS_KEY=<username>:<password>
           export RS_ENDPOINT=https://<DRP IP>:8092
        
        For example:
        
        .. code:: sh
        
           export RS_KEY='rocketskates:r0cketsk8ts'
           export RS_ENDPOINT=https://10.10.10.1:8092
     
  

  .. tab:: Windows

  
     
     #. Open the `files <https://portal.rackn.io/#/e/0.0.0.0/files>`__ view
     
     
     #. Click ``drpcli.amd64.windows`` to download
     
     
     #. Open a powershell terminal
     
     
     #. Put the binary in place and make executable
     
        .. code:: powershell
        
           cd Downloads
           cp drpcli.exe c:\Windows
           cp drpcli.exe c:\Windows\jq.exe
     
     
     #. Set the endpoint and access credentials
     
        During the lab1000, you may have changed the access username and
        password. You will need to use those now to set an environment variable
        for accessing DRP.
        
        If you used the `Terraform
        Setup <https://docs.rackn.io/en/latest/doc/install/install-cloud.html#quickstart?utm_source=trial>`__,
        then these values will be provided
        
        .. code:: powershell
        
           $env:RS_KEY = '<username>:<password>'
           $env:RS_ENDPOINT = 'https://<DRP IP>:8092'
        
        For example:
        
        .. code:: powershell
        
           $env:RS_KEY = 'rocketskates:r0cketsk8ts'
           $env:RS_ENDPOINT = 'https://10.10.10.1:8092'
     
  


Check Versions
++++++++++++++
   
   
   
   #. Check ``drpcli`` version
   
      .. code:: sh
      
         drpcli version
      
      This should display something like:
      
      .. code:: sh
      
         Version: v4.10.0
   
   
   
   
   #. Check ``jq`` version
   
      .. code:: sh
      
         jq -v
      
      This should display something like:
      
      .. code:: sh
      
         gojq 0.12.3 (rev: HEAD/go1.18.2)
   
   
   
   
   #. Redo the previous ``Setup CLI Environment`` step if those commands fail
   


Test Access
+++++++++++
   
   
   
   #. Make sure that the environment is configured correctly
   
      .. code:: sh
      
         drpcli info get | jq -r .version
      
      This should display something like:
      
      ::
      
         v4.10.0
   
   
   
   
   #. Redo the previous ``Setup CLI Environment`` if this commands fail
   


Experiment with Alerts
++++++++++++++++++++++
   
   
   
   #. Create an alert
   
      .. code:: sh
      
         drpcli alerts post INFO lab2000
   
   
   
   
   #. Navigate to `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__
   
   
   
   
   #. See the newly created alert in the table
   
      The table will live update as other things change.
   
   
   
   
   #. List the alerts from the CLI
   
      .. code:: sh
      
         drpcli alerts list Name=lab2000
   
   
   
   
   #. Acknowledge the Alert from the CLI
   
      .. code:: sh
      
         drpcli alerts ack `drpcli alerts list Name=lab2000 | jq -rc 'head|.Uuid'`
      
      Note the two ``drpcli`` calls. The inner ``drpcli`` call is used to get
      the Uuid of the most recent alert. The outer ``drpcli`` call is used to
      acknowledge it.
   


Playing with Machines with the CLI
++++++++++++++++++++++++++++++++++
   
   
   
   #. Create a machine from the CLI
   
      .. code:: sh
      
         drpcli machines create lab2000
   
   
   
   
   #. Look at the `machine <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2000>`__ in the Portal
   
      The machine view will update as the CLI makes changes.
   
   
   
   
   #. Show the machine in the CLI
   
      .. code:: sh
      
         drpcli machines show Name:lab2000
      
      This will show the JSON object for that machine.
   
   
   
   
   #. Set a workflow on the machine
   
      .. code:: sh
      
         drpcli machines workflow Name:lab2000 universal-start
      
      This will show the JSON object for that machine.
   
   
   
   
   #. Create a context container for the machine
   
      .. code:: sh
      
         drpcli machines update Name:lab2000 '{ "Context": "drpcli-runner" }'
      
      This will show the JSON object for that machine.
   
   
   
   
   #. Notice the `machine <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2000/activity>`__ run the
      universal-start workflow
   
   
   
   
   #. Count the machines in the system
   
      .. code:: sh
      
         drpcli machines count
      
      This will show the number machines in the system. Something like: ``2``.
   
   
   
   
   #. Use the CLI to wait for changes in the object
   
      .. code:: sh
      
         drpcli machines await Name:lab2000 'Description=Eq(lab2000)'
      
      This command will wait until the machine’s description changes.
      
      Go to the `machine <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab2000>`__, change the
      Description, and click in the white space around the description.
      
      The command returns ``complete``.
   
   
   
   
   #. Remove the machine
   
      .. code:: sh
      
         drpcli machines cleanup Name:lab2000
      
      This returns that the machine is deleted. Notice the
      `machine <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ is removed from the Portal.
   
   
   
   
   #. Count the machines in the system (again)
   
      .. code:: sh
      
         drpcli machines count
      
      This will show the number machines in the system. Something like: ``2``
      The value is one less than before.
   


Watch for Events from the CLI
+++++++++++++++++++++++++++++
   
   
   
   #. Install the dev-library content pack
   
      .. code:: sh
      
         drpcli catalog item install dev-library --version=tip
      
      A JSON object of the ``dev-library`` content pack will be returned.
   
   
   
   
   #. Subscribe for events
   
      .. code:: sh
      
           drpcli events watch contents.*.dev-library
      
      This command will run until stopped.
   
   
   
   
   #. Navigate to `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__
   
   
   
   
   #. Select ``dev-library`` row and click ``Delete``
   
   
   
   
   #. Watch for the update events.
   
      In the shell, the previously running command will show a JSON event
      indicating that the content pack was deleted.
   
   
   
   
   #. Stop the event watcher in the shell by pressing cntrl-C
   



